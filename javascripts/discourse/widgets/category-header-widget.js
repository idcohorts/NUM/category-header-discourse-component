import { getOwner } from "discourse-common/lib/get-owner";
import { h } from "virtual-dom";
import { iconNode } from "discourse-common/lib/icon-library";
import { createWidget } from "discourse/widgets/widget";
import Category from "discourse/models/category";
import Topic from "discourse/models/topic";

function buildCategory(category, widget) {
  const content = [];

  if (settings.show_category_icon) {
    try {
      content.push(widget.attach("category-icon", { category }));
    } catch {
      // if widget attaching fails, ignore it as it's probably the missing component
    }
  }

  let categoryTitle = category.read_restricted
    ? [iconNode("lock"), category.name]
    : category.name;

  content.push(h("h1.category-title", categoryTitle));

  if (settings.show_description) {
    content.push(
      h(
        "div.category-title-description",
        h("div.cooked", { innerHTML: category.description })
      )
    );
  }

  return content;
}

var cat_count = 0;

function addCategory(category) {
  
  const check_div = document.getElementsByClassName("category-title-description");
  console.log(check_div)
  if (check_div.length === 1) return;

  console.log(cat_count)
  if (cat_count >0) return;

  const div = document.getElementsByClassName("category-title-contents")[0];

  console.log("div", div)
  console.log("category", category)

  let categoryTitle = category.name;
  console.log("category", categoryTitle)

  var h1 = document.createElement("h1");
  h1.setAttribute("class", "category-title");
  h1.innerText = categoryTitle;

  var content_div = document.createElement("div");
  content_div.setAttribute("class", "category-title-description");
  var cooked_div = document.createElement("div");
  cooked_div.setAttribute("class", "cooked");
  cooked_div.innerHTML = category.description;
  content_div.appendChild(cooked_div);

  div.appendChild(h1);
  div.appendChild(content_div);
  cat_count++;
}


function getTopic(route){  
  const topic_slug = route.parent.params.slug;
  const topic_id = route.parent.params.id;
  let topic = Topic.find(
    topic_slug, topic_id
  );
  
  topic.then(topic_obj => {

    console.log("topic", topic_obj);
        
    const category = Category.findById(
      topic_obj.category_id
    );
    console.log("category", category);
    
    addCategory(category);
  });
}


export default createWidget("category-header-widget", {
  tagName: "span.discourse-category-banners",

  html() {

    const check_div = document.getElementsByClassName("category-title-description");
    if(check_div.length> 0){
      throw "not to be handled";
    }
    
      const router = getOwner(this).lookup("router:main");
      const route = router.currentRoute;
      if (
        route &&
        route.params &&
        route.params.hasOwnProperty("category_slug_path_with_id")
      ) {
        const categories = {};

        settings.categories.split("|").forEach((item) => {
          item = item.split(":");

          if (item[0]) {
            categories[item[0]] = item[1] || "all";
          }
        });

        const category = Category.findBySlugPathWithID(
          route.params.category_slug_path_with_id
        );
        console.log("category promise", category)

        const isException = settings.exceptions
          .split("|")
          .filter(Boolean)
          .includes(category.name);
        const isTarget =
          Object.keys(categories).length === 0 ||
          categories[category.name] === "all" ||
          categories[category.name] === "no_sub" ||
          (category.parentCategory &&
            (categories[category.parentCategory.name] === "all" ||
              categories[category.parentCategory.name] === "only_sub"));
        const hideMobile = !settings.show_mobile && this.site.mobileView;
        const isSubCategory =
          !settings.show_subcategory && category.parentCategory;
        const hasNoCategoryDescription =
          settings.hide_if_no_description && !category.description_text;

        if (
          isTarget &&
          !isException &&
          !hasNoCategoryDescription &&
          !isSubCategory &&
          !hideMobile
        ) {
          document.body.classList.add("category-header");

          console.log("route",route);
          console.log("settings", settings);
          console.log("category", category);
          return h(
            `div.category-title-header.category-banner-${category.slug}`,
            {
              attributes: {
                style: `background-color: #${category.color}; color: #${category.text_color};`,
              },
            },
            h("div.category-title-contents", buildCategory(category, this))
          );
        }
      } else {
        // console.log(JSON.stringify(Object.keys(route)));
        // console.log(JSON.stringify(Object.keys(settings)));
        // console.log(JSON.stringify(settings));
        console.log("top", Topic);
        console.log("cat", Category);

        let categories = Category.list();
        console.log("cate", categories);
        
        getTopic(route);            

        return h(
            `div.category-title-header.category-banner`,
            h("div.category-title-contents")
            );          
      }      
  },
});
